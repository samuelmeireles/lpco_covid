'use strict'

const fs = require('fs');
const pegaTokens = require('./utils/autentica.js').pegaTokens;
const get = require('./utils/get.js').get;
const PromisePool = require('es6-promise-pool');
const configs = require('./utils/config.js').parametros;

const MAXIMO_REQUESTS_CONCORRENTES = configs["max-requests-concorrentes"];
const ambiente = process.env.ENV || 'prod';

let	NUMERO_MODELO_LPCO;
let	urlBase;
let	systemCode;
let	nomeArquivo;

if (ambiente === "hom") {
	NUMERO_MODELO_LPCO = configs["modelo-lpco"].hom;
	urlBase = configs["url-base"].hom;
	systemCode = configs['system-code'].hom;
	nomeArquivo = configs["nome-arquivo-jsons"].hom;
}
if (ambiente === "prod") {
	NUMERO_MODELO_LPCO = configs["modelo-lpco"].prod;
	urlBase = configs["url-base"].prod;	
	systemCode = configs['system-code'].prod;
	nomeArquivo = configs["nome-arquivo-jsons"].prod;
}

const urlConsultaSimples = `${urlBase}/talpco/api/ext/lpco/consulta?orgao-anuente=DECEX&codigo-modelo=${NUMERO_MODELO_LPCO}&offset=`;
const urlconsultaDetalhada = `${urlBase}/talpco/api/ext/lpco`;

async function main () {

	console.log(`Autenticando em ${ambiente}`);
	let tokens;

	try {
		tokens = await pegaTokens({systemCode, urlBase});
		tokens.systemCode = systemCode;
	} catch (e) {
		return console.error(`Erro na autenticação: ${e}`);
	}

	console.log(`Buscando informações dos LPCOs do modelo ${NUMERO_MODELO_LPCO}`);
	const primeiraPagina = 0;
	let chamada = await get(tokens, `${urlConsultaSimples}${primeiraPagina}`);
	const numeroTotalLpcos = chamada.headers['content-range'].split('/')[1];
	const numeroDePaginas = Math.ceil(numeroTotalLpcos/500);

	console.log(`Data/Hora: ${new Date().toString()}`);
	console.log(`Total de LPCOs: ${numeroTotalLpcos}`);
	console.log('Iniciando a busca dos LPCOs...');

	let lpcosSimples = JSON.parse(chamada.body);
	for (let i = 1; i < numeroDePaginas; i++) {
		
		chamada = await get(tokens, `${urlConsultaSimples}${i*500}`);
		JSON.parse(chamada.body).map(function (lpco) {
			lpcosSimples.push(lpco);
		});
	}

	let linhas = [];
	let numerosLpco = [];

	lpcosSimples.map((lpco) => {
		linhas.push(`${lpco.numero};${lpco.dataRegistro};`);
		numerosLpco.push(`${lpco.numero}`);
	});

	let lpcosCompletos = [];

	let erros = false;
	let progresso = 0;
	let progressoPorcentagemAnterior = "0%"
	let progressoPorcentagemNovo = '0%';

	function pegaLPCO(numeroDoLpco) {
		return get(tokens, `${urlconsultaDetalhada}/${numeroDoLpco}`).then((result) => {
			if (erros) return;
			let statusCode = result.statusCode;

			progresso++;
			progressoPorcentagemNovo = `${Math.round((progresso/numeroTotalLpcos)*100)}%`;

			if (progressoPorcentagemNovo !== progressoPorcentagemAnterior) {
				console.log(`Progresso: ${progressoPorcentagemNovo}`);
			}

			progressoPorcentagemAnterior = progressoPorcentagemNovo;

			if (statusCode === 200) {
				let body = JSON.parse(result.body);
				lpcosCompletos.push(body);
			} else {
				erros = true;
				throw new Error(`LPCO: ${numeroDoLpco}: ${statusCode}`);
			}

		}, function (err) {
			throw new Error(err);
		});
	}

	function montaFila () {
		if (numerosLpco.length > 0) {
			let numeroDoLpco = numerosLpco.pop();
			return pegaLPCO(numeroDoLpco);
		} else {
			return null;
		}
	}

	var pool = new PromisePool(montaFila, MAXIMO_REQUESTS_CONCORRENTES);

	let poolPromise = pool.start();

	poolPromise.then(function () {
		fs.writeFileSync(`./arquivos/${nomeArquivo}`, JSON.stringify(lpcosCompletos));
		console.log(`Data/Hora: ${new Date().toString()}`);
		return console.log('LPCOs gravados com sucesso');
	}, function (err) {
		return console.error(err);
	});
}

main();