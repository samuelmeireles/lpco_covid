## Rodar o comando

`$ npm install` (vai instalar as bibliotecas necessárias para rodar o código);

## Certificado

Incluir o certificado com o nome "MDICGOVBR.p12" na pasta "arquivos/certificados" (o nome pode ser alterado nas configurações)

## Incluir as váriveis de ambiente:

- senha_cert_prod (senha do certificado de produção)
- system_code_prod (system code da produção)

## Rodar os scripts

`node pegaLpcos.js` vai extrair os LPCOs registrados até o momento (eles são salvos no arquivo "lpcosCompletos.json")

`node processaLpcos.js` vai processar o arquivo json acima e gerar o arquivo "LPCO_COVID.csv"

`node pegaUnidadesEmbarque.js` vai extrair as unidades de embarque de todas as DUEs vinculadas aos LPCOs

Obs.: Os arquivos gerados são salvos salvos na pasta "arquivos"

## Opcional:

Editando o arquivo utils/config.js é possível alterar alguns parâmetros (máximo de requests concorrentes, nomes dos arquivos gerados e os nomes dos certificados)