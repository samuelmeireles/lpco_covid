'use strict'

const request = require('request');
const fs = require('fs');
const Q = require('q');

const abrirArquivo = fs.readFileSync;

const pastaCerts = './arquivos/certificados';
const cadeiaSerpro = [abrirArquivo(`${pastaCerts}/icp.cer`),
					  abrirArquivo(`${pastaCerts}/serprov3.cer`),
					  abrirArquivo(`${pastaCerts}/serproacfv3.cer`)];

const configs = require('./config.js').parametros;
const ambiente = process.env.ENV || 'prod';

let certDECEX;
let senhaCert;
if (ambiente === "hom") {
	certDECEX = abrirArquivo(`${pastaCerts}/${configs["nome-certificado"].hom}`);
	senhaCert = configs["senha-certificado"].hom;
}
if (ambiente === "prod") {
	certDECEX = abrirArquivo(`${pastaCerts}/${configs["nome-certificado"].prod}`);
	senhaCert = configs["senha-certificado"].prod;
}

function autenticar (dados) {

	let q = Q.defer();

	let urlBase = dados.urlBase ? dados.urlBase : 'https://anuentes.portalunico.siscomex.gov.br';

	var options = {
		url: `${urlBase}/portal/api/autenticar/sistema`,
		agentOptions: {
			ca: cadeiaSerpro,
			pfx: certDECEX,
			passphrase: senhaCert
		},
		headers: {
			'System-Code': dados.systemCode
		}
	}

	request.post(options, function (err, resp, body) {
		const statusCode = resp.statusCode;

		if (err) return q.reject(err);
		if (statusCode !== 200) {
			let mensagem = JSON.parse(body).message;
			q.reject(mensagem);
		}

		const headers = resp.headers;

		q.resolve({set: headers['set-token'], csrf: headers['x-csrf-token']});
	});

	return q.promise;
};

exports.pegaTokens = async function pegaTokens (dados) {
	try {
		const tokens = await autenticar(dados || {});

		const set = tokens.set;
		const csrf = tokens.csrf;

		return {setToken: set, csrfToken: csrf};
	} catch (e) {
		throw(e);
	}
}