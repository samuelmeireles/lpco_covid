'use strict'

const fs = require('fs');
const ncmsAtts = fs.readFileSync('./arquivos/atts.csv', 'utf-8').split('\r\n');

exports.pegaAtributos = function pegaAtributos (ncm, atributosNcm) {

	let atributos = [];

	atributosNcm.map(function (atributo) {
		let codigoAtributo = atributo.codigo;
		let valorAtributo = atributo.listaValor[0];

		ncmsAtts.map(function (ncmAtt) {
			if (ncmAtt.split(';')[0] === ncm && codigoAtributo === ncmAtt.split(';')[1]) {
				atributos.push(pegaDescricaoAtributo(ncm, codigoAtributo, valorAtributo));
			}
		});
	});
	
	return atributos;
}

function pegaDescricaoAtributo (ncm, att, valor) {
	let atributosPorNcm = JSON.parse(fs.readFileSync('./arquivos/atributos_por_ncm.json', 'utf-8'));
	let descricao;
	atributosPorNcm.listaNcm.map(function (valores) {
		if (valores.codigoNcm.split('.').join('') === ncm) {
			valores.listaAtributos.map(function (atributo) {
				if ((atributo.codigo) === att) {
					atributo.dominio.map(function (dominio) {
						if (dominio.codigo === valor) {
							descricao = dominio.descricao;
						}
					});
				}
			});
		}
	});
	return descricao;
}