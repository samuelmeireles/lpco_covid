'use strict'

const request = require('request');
const fs = require('fs');
const Q = require('q');

const abrirArquivo = fs.readFileSync;

const pastaCerts = './arquivos/certificados';
const cadeiaSerpro = [abrirArquivo(`${pastaCerts}/icp.cer`),
					  abrirArquivo(`${pastaCerts}/serprov3.cer`),
					  abrirArquivo(`${pastaCerts}/serproacfv3.cer`)];

const configs = require('./config.js').parametros;
const ambiente = process.env.ENV || 'prod';

let certDECEX;
let senhaCert;
if (ambiente === "hom") {
	certDECEX = abrirArquivo(`${pastaCerts}/${configs["nome-certificado"].hom}`);
	senhaCert = configs["senha-certificado"].hom;
}
if (ambiente === "prod") {
	certDECEX = abrirArquivo(`${pastaCerts}/${configs["nome-certificado"].prod}`);
	senhaCert = configs["senha-certificado"].prod;
}

exports.get = async function get (tokens, url) {
	let q = Q.defer();
	
	var options = {
		url: url,
		agentOptions: {
			ca: cadeiaSerpro,
			pfx: certDECEX,
			passphrase: senhaCert
		},
		headers: {
			'Authorization': tokens.setToken,
			'X-CSRF-Token': tokens.csrfToken,
			'System-Code': tokens.systemCode
		},
		timeout: 120000
	}

	request.get(options, function (err, resp, body) {
		if (err) {
			return q.reject(err.code);
		}

		const headers = resp.headers;
		const statusCode = resp.statusCode;

		q.resolve({headers, body, statusCode});
	});

	return q.promise;
}