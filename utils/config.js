exports.parametros = {
	"system-code": {
		"hom": process.env.system_code_hom,
		"prod": process.env.system_code_prod
	},
	"url-base": {
		"hom": 'https://hom-mdic.pucomex.serpro.gov.br',
		"prod": 'https://anuentes.portalunico.siscomex.gov.br'
	},
	"max-requests-concorrentes": 100,
	"modelo-lpco": {
		"hom": "E00571",
		"prod": "E00115"
	},
	"nome-arquivo-jsons": {
		"hom": "lpcosCompletos_HOM.json",
		"prod": "lpcosCompletos.json"
	},
	"nome-arquivo-lpcos": {
		"hom": "LPCO_COVID_HOM.csv",
		"prod": "LPCO_COVID.csv"
	},
	"nome-arquivo-uas": {
		"hom": "uas_HOM.csv",
		"prod": "uas.csv"
	},
	"senha-certificado": {
		"hom": process.env.senha_cert_hom,
		"prod": process.env.senha_cert_prod
	},
	"nome-certificado": {
		"hom": "MDICGOVBR.p12",
		"prod": "MDICGOVBR.p12"
	}
}