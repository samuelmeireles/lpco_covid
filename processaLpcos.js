'use strict'

const fs = require('fs');
const pegaAtributos = require('./utils/pegaAtributos.js').pegaAtributos;
const configs = require('./utils/config.js').parametros;

const ambiente = process.env.ENV || 'prod';
let	nomeArquivoLeitura;
let nomeArquivoGravacao;

async function main () {

	if (ambiente === "hom") {
		nomeArquivoLeitura = configs["nome-arquivo-jsons"].hom;
		nomeArquivoGravacao = configs["nome-arquivo-lpcos"].hom;
		processaLPCOSHom();
	}
	if (ambiente === "prod") {
		nomeArquivoLeitura = configs["nome-arquivo-jsons"].prod;
		nomeArquivoGravacao = configs["nome-arquivo-lpcos"].prod;
		processaLPCOSProd();
	}

}

function processaLPCOSProd () {

	console.log(`Data/Hora: ${new Date().toString()}`);
	console.log('Iniciando...');

	let lpcosCompletos = [];
	lpcosCompletos = JSON.parse(fs.readFileSync(`./arquivos/${nomeArquivoLeitura}`, 'utf-8'));

	let linhasGravar = ['Número LPCO;Data de Registro;CNPJ;Razão Social;Situação;DUEs Vinculadas;Número do Item;NCM;Atributos;Descrição;País de Destino;Quantidade Estatística;Unidade de Medida;Peso (KG)'];

	let progresso = 0;
	let progressoPorcentagemAnterior = "0%"
	let progressoPorcentagemNovo = '0%';

	console.log('Processando...');
	lpcosCompletos.map(function (lpcoCompleto, idxCompleto) {

		progresso++;
		progressoPorcentagemNovo = `${Math.round((progresso/lpcosCompletos.length)*100)}%`;

		if (progressoPorcentagemNovo !== progressoPorcentagemAnterior) {
			console.log(`Progresso: ${progressoPorcentagemNovo}`);
		}

		progressoPorcentagemAnterior = progressoPorcentagemNovo;

		let itensLpco = lpcoCompleto.listaNcm;
		let itensLpcoString = [];
		itensLpco.map(function (item) {
			let numeroItem = item.numeroItem;
			let ncm = item.ncm;
			let camposItem = item.listaCamposNcm;
			let descricao = camposItem[0].listaValor[0];
			let paisDestino = camposItem[1] ? camposItem[1].listaValor[0] : ' ';
			let qtd = camposItem[2] ? `${camposItem[2].listaValor[0].split('.').join(',')}` : ' ';
			let unidadeMedida = camposItem[2] ? `${camposItem[2].unidadeMedida}` : ' ';
			let peso = camposItem[3] ? camposItem[3].listaValor[0].split('.').join(',') : ' ';
			let atributosNcm = item.listaAtributosNcm;
			let atributos = pegaAtributos(ncm, atributosNcm);


			itensLpcoString.push(`${numeroItem};${ncm};${atributos.join(', ')};${descricao.split('\n').join(' ').split(';').join(' ').split('-').join(' - ')};${paisDestino};${qtd};${unidadeMedida};${peso}`);
		});

		let duesVinculadas = '';
		if (lpcoCompleto.listaVinculos.length > 0) {
			lpcoCompleto.listaVinculos.map(function (vinculo) {
				duesVinculadas += `${vinculo.numeroDocumento}-${vinculo.numeroDocumentoItem}, `;
			});
		} else {
			duesVinculadas = ' ';
		}

		let exportador = lpcoCompleto.listaCamposFormulario[0].intervenientes[0].nome;

		let numeroLpco = lpcoCompleto.numero;
		let dataRegistro = lpcoCompleto.dataRegistro;
		let complementoLinha = '';
		itensLpcoString.map(function (linhaAComplementar) {
			linhasGravar.push(`${numeroLpco};${dataRegistro};${lpcoCompleto.importadorExportador};${exportador};${lpcoCompleto.situacao.descricao};${duesVinculadas};${linhaAComplementar}`);
		});
	});

	fs.writeFileSync(`./arquivos/${nomeArquivoGravacao}`, linhasGravar.join('\r\n'));

	console.log(`Data/Hora: ${new Date().toString()}`);
	console.log(`Processamento concluído. Gerado o arquivo ${nomeArquivoGravacao}`);

}

main();