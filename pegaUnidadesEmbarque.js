'use strict'

const fs = require('fs');
const PromisePool = require('es6-promise-pool');
const pegaAtributos = require('./utils/pegaAtributos.js').pegaAtributos;
const configs = require('./utils/config.js').parametros;
const pegaTokens = require('./utils/autentica.js').pegaTokens;
const get = require('./utils/get.js').get;
const MAXIMO_REQUESTS_CONCORRENTES = configs["max-requests-concorrentes"];

const ambiente = process.env.ENV || 'prod';
let	nomeArquivoLeitura;
let nomeArquivoGravacao;
let urlBase;
let	systemCode;

async function main () {

	if (ambiente === "hom") {
		nomeArquivoLeitura = configs["nome-arquivo-jsons"].hom;
		nomeArquivoGravacao = configs["nome-arquivo-uas"].hom;
		urlBase = configs["url-base"].hom;
		systemCode = configs['system-code'].hom;
	}
	if (ambiente === "prod") {
		nomeArquivoLeitura = configs["nome-arquivo-jsons"].prod;
		nomeArquivoGravacao = configs["nome-arquivo-uas"].prod;
		urlBase = configs["url-base"].prod;
		systemCode = configs['system-code'].prod;
	}
	console.log(`Autenticando em ${ambiente}`);

	let tokens;

	try {
		tokens = await pegaTokens({systemCode, urlBase});
		tokens.systemCode = systemCode;
	} catch (e) {
		return console.error(`Erro na autenticação: ${e}`);
	}

	const urlconsultaDue = `${urlBase}/due/api/ext/due/consultarDadosResumidosDUE`;

	let lpcosCompletos = [];
	lpcosCompletos = JSON.parse(fs.readFileSync(`./arquivos/${nomeArquivoLeitura}`, 'utf-8'));

	let dues = new Set();
	lpcosCompletos.map(function (lpco) {
		lpco.listaVinculos.map(function (vinculo) {
			dues.add(vinculo.numeroDocumento);
		});
	});

	dues = Array.from(dues);
	dues[0] = "20BR0000000468"

	let uas = new Set();

	let progresso = 0;
	let progressoPorcentagemAnterior = "0%"
	let progressoPorcentagemNovo = '0%';
	let numeroTotalDues = dues.length;
	let erros = false;

	function pegaUa(numeroDaDue) {
		return get(tokens, `${urlconsultaDue}?numero=${numeroDaDue}`).then((result) => {
			if (erros) return;
			let statusCode = result.statusCode;

			progresso++;
			progressoPorcentagemNovo = `${Math.round((progresso/numeroTotalDues)*100)}%`;

			if (progressoPorcentagemNovo !== progressoPorcentagemAnterior) {
				console.log(`Progresso: ${progressoPorcentagemNovo}`);
			}

			progressoPorcentagemAnterior = progressoPorcentagemNovo;

			if (statusCode === 200) {
				let body = JSON.parse(result.body);
				let unidadeDespacho = body.uaDespacho;
				uas.add(`${numeroDaDue},${unidadeDespacho}`);
			} else {
				erros = true;
				throw new Error(`DUE: ${numeroDaDue}: ${statusCode}`);
			}

		}, function (err) {
			throw new Error(err);
		});
	}

	function montaFila () {
		if (dues.length > 0) {
			let numeroDaDue = dues.pop();
			return pegaUa(numeroDaDue);
		} else {
			return null;
		}
	}

	var pool = new PromisePool(montaFila, MAXIMO_REQUESTS_CONCORRENTES);

	let poolPromise = pool.start();

	poolPromise.then(function () {
		uas = Array.from(uas);
		let arquivo = processaArquivo(uas, lpcosCompletos);

		fs.writeFileSync(`./arquivos/${nomeArquivoGravacao}`, arquivo.join('\r\n'));
		console.log(`Data/Hora: ${new Date().toString()}`);
		return console.log(`Processamento concluído. Gerado o arquivo ${nomeArquivoGravacao}`);
	}, function (err) {
		return console.error(`Houve erro no processamento: ${err.message}`);
	});

	function processaArquivo(uas, lpcos) {
		let arquivo = ['Número do LPCO,Número da DUE, Unidade de Despacho'];
		lpcos.map(lpco => {
			let duesVinculo = [];
			lpco.listaVinculos.map(vinculo => {
				duesVinculo.push(vinculo.numeroDocumento);
			});
			uas.map((ua, idx) => {
				let dueUa = ua.split(',')[0];
				if (duesVinculo.indexOf(dueUa) !== -1) {
					arquivo.push(`${lpco.numero},${ua}`);
				}
			})
		});
		return arquivo;
	}
}

main();